{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "ekrut-package.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "ekrut-package.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "ekrut-package.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "ekrut-package.labels" -}}
helm.sh/chart: {{ include "ekrut-package.chart" . }}
{{ include "ekrut-package.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "ekrut-package.selectorLabels" -}}
app.kubernetes.io/name: {{ include "ekrut-package.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{/*
Liveness Probe
*/}}
{{- define "ekrut-package.livenessScript" -}}
{{- printf "[ $(curl -I 0.0.0.0:%.0f 2>/dev/null | head -n 1 | cut -d$' ' -f2) -gt 0 ] && exit 0 || exit 1" .Values.container.port -}}
{{- end -}}

{{/*
Readiness Probe
*/}}
{{- define "ekrut-package.readinessScript" -}}
{{- printf "[ $(curl -I 0.0.0.0:%.0f 2>/dev/null | head -n 1 | cut -d$' ' -f2) -lt 500 ] && exit 0 || exit 1" .Values.container.port -}}
{{- end -}}

